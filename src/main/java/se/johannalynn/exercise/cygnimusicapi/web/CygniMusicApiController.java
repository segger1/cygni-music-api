package se.johannalynn.exercise.cygnimusicapi.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashup;
import se.johannalynn.exercise.cygnimusicapi.service.MashupService;
import se.johannalynn.exercise.cygnimusicapi.util.CygniMusicApiValidation;

@RestController
@RequestMapping("/")
@Api(value="api", description="Music API operations")
public class CygniMusicApiController {

    @Autowired
    private MashupService service;

    private static final String template = "Hello, %s";

    @ApiOperation(value = "Say hello", produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hello message from the server to name given in query")
    })
    @GetMapping("/hello")
    @ResponseBody
    public String greeting(@RequestParam(value="name", defaultValue="Anonymous") String name) {
        return String.format(template, name);
    }

    @ApiOperation(value = "Retrieve mashup information of artist")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of artist mashup information"),
            @ApiResponse(code = 400, message = "MBID is not valid", response = RestErrorInfo.class),
            @ApiResponse(code = 404, message = "Artist is not found", response = RestErrorInfo.class),
            @ApiResponse(code = 500, message = "Server couldn't process request", response = RestErrorInfo.class),
            @ApiResponse(code = 502, message = "Subsystem couldn't process request", response = RestErrorInfo.class)
    })
    @GetMapping(value = "/mashup/{mbid}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CygniMusicMashup getMashup(@PathVariable(value="mbid") final String mbid) {
        if(!CygniMusicApiValidation.validMbid(mbid)) throw new InvalidInputException("[ " + mbid + " ] is not a valid MBID");
        return service.getMashup(mbid);
    }
}
