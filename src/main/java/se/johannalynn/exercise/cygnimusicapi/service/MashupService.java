package se.johannalynn.exercise.cygnimusicapi.service;

import se.johannalynn.exercise.cygnimusicapi.integration.exception.SubsystemException;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashup;
import se.johannalynn.exercise.cygnimusicapi.service.exception.AlbumHandlingException;
import se.johannalynn.exercise.cygnimusicapi.service.exception.ArtistNotFoundException;

public interface MashupService {

    /**
     * Get a mashup with information about the artist defined by MBID
     *
     * @param mbid - MusicBranzIdentifier
     * @return {@link CygniMusicMashup}
     * @throws SubsystemException when subsystem couldn't serve the request
     * @throws AlbumHandlingException when thread problems occurred during album fetching
     * @throws ArtistNotFoundException when artist does not exists in MusicBranz
     */
    CygniMusicMashup getMashup(final String mbid) throws SubsystemException, AlbumHandlingException, ArtistNotFoundException;
}
