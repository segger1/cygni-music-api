package se.johannalynn.exercise.cygnimusicapi.web;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@ApiIgnore
public class RestErrorController implements ErrorController {

    private static final String ERROR_PATH = "/error";

    @RequestMapping(ERROR_PATH)
    @ResponseBody
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    public RestErrorInfo defaultError() {
        String message = "Any attempt to brew coffee with a teapot should result "
                        + "in the error code 418 I'm a teapot. The resulting "
                        + "entity body MAY be short and stout.";
        return new RestErrorInfo("", message);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
