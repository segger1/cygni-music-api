package se.johannalynn.exercise.cygnimusicapi.web;

import io.swagger.annotations.ApiModelProperty;

public class RestErrorInfo {

    @ApiModelProperty(notes = "Name of exception", required = true)
    private String exception;

    @ApiModelProperty(notes = "Error message", required = true)
    private String message;

    public RestErrorInfo(String exception, String message) {
        this.exception = exception;
        this.message = message;
    }

    public String getException() {
        return exception;
    }

    public String getMessage() {
        return message;
    }
}
