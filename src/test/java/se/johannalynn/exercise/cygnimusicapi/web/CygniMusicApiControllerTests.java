package se.johannalynn.exercise.cygnimusicapi.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashup;
import se.johannalynn.exercise.cygnimusicapi.service.MashupService;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CygniMusicApiControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MashupService mockService;

    @Test
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Hello, Anonymous")));
    }

    @Test
    public void getHelloParams() throws Exception {
        final String name = "name";
        mvc.perform(MockMvcRequestBuilders.get("/hello?name=" + name))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Hello, " + name)));
    }

    @Test
    public void getMashup() throws Exception {
        final String mbid = "d13f0f47-36f9-4661-87fe-2de56f45c649";

        when(mockService.getMashup(mbid)).thenReturn(getMockMashup(mbid));

        mvc.perform(MockMvcRequestBuilders.get("/mashup/{mbid}", mbid))
                .andExpect(status().isOk())
                .andExpect(jsonPath("mbid", is(mbid)));

    }

    @Test
    public void getMashupInvalidInput() throws Exception {
        final String invalidMbid = "invalidMbid";

        String message = "[ %s ] is not a valid MBID";

        mvc.perform(MockMvcRequestBuilders.get("/mashup/{mbid}", invalidMbid))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("exception", is("InvalidInputException")))
                .andExpect(jsonPath("message", is(String.format(message, invalidMbid))));

    }

    private CygniMusicMashup getMockMashup(String mbid) {
        CygniMusicMashup mashup = new CygniMusicMashup();
        mashup.setMbid(mbid);
        return mashup;
    }
}
