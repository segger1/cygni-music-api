package se.johannalynn.exercise.cygnimusicapi.integration;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.NotFoundException;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubsystemHttpClientTests {

    @Autowired
    private SubsystemHttpClient unitUnderTest;

    @MockBean
    private CloseableHttpClient httpClient;

    @MockBean
    private CloseableHttpResponse httpResponse;

    @MockBean
    private StatusLine statusLine;

    @MockBean
    private HttpEntity httpEntity;

    @Test
    public void getSuccessRequest() throws IOException, NotFoundException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(httpResponse.getEntity()).thenReturn(httpEntity);

        final String coverartFile = "/coverart.json";
        InputStream coverartIs = this.getClass().getResourceAsStream(coverartFile);
        when(httpEntity.getContent()).thenReturn(coverartIs);

        String imageUrl = unitUnderTest.get("search", new CoverArtHandler("http://base-path"));
        assertEquals("http://coverartarchive.org/release/a146429a-cedc-3ab0-9e41-1aaf5f6cdc2d/3012495605.jpg", imageUrl);
    }

    @Test(expected = NotFoundException.class)
    public void getNoSearchParameter() throws IOException, NotFoundException {
        final String search = null;
        unitUnderTest.get(search, new CoverArtHandler("http://base-path"));
    }

    @Test(expected = NotFoundException.class)
    public void getInvalidPath() throws IOException, NotFoundException {
        final String basePath = "invalid";
        unitUnderTest.get("search", new CoverArtHandler(basePath));
    }

    @Test(expected = NotFoundException.class)
    public void statusCodeError() throws IOException, NotFoundException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(404);

        unitUnderTest.get("search", new CoverArtHandler("http://base-path"));
    }

    @Test(expected = NotFoundException.class)
    public void noEntityInResponse() throws IOException, NotFoundException {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(httpResponse);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(httpResponse.getEntity()).thenReturn(null);

        unitUnderTest.get("search", new CoverArtHandler("http://base-path"));
    }

    @Test(expected = IOException.class)
    public void serverFailure() throws IOException, NotFoundException {
        when(httpClient.execute(any(HttpGet.class))).thenThrow(new IOException());

        unitUnderTest.get("search", new CoverArtHandler("http://base-path"));
    }
}
