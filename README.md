# cygni-music-api
This is a mashup API that gives the user information about an artist (defined by MBID), such as albums and a description.

## Table of contents
* [Build & run](#build-run)
	- [Development](#development-mode)
	- [Production](#production-mode)
* [API Overview](#api-overview)
* [Description](#description)
	- [My solution](#my-solution)

## Build & run
Download the source code. Either by cloning this repo or download the source as archive and unpack it. Enter the `cygni-music-api` folder.

Build the an executable file with
```
mvn clean package
```

Start with
```
java -jar target/cygni-music-api-1.2.0.jar
```

Try out the server at [http://localhost:8080/cygni-music-api/hello](http://localhost:8080/cygni-music-api/hello)

Try out the mashup with one of my favorite artists (Tegan & Sara) at [http://localhost:8080/cygni-music-api/mashup/d13f0f47-36f9-4661-87fe-2de56f45c649](http://localhost:8080/cygni-music-api/mashup/d13f0f47-36f9-4661-87fe-2de56f45c649)

##### Development mode
To run the application in development mode (some more logging)
```
java -jar -Dspring.profiles.active=development target/cygni-music-api-1.2.0.jar
```

Logs are available in `cygni-music-api/logs`

* `application.log`/`application-error.log` - main log for application, level `ERROR` in production
* `cygnimusicapi.log` - application specific classes, level `WARN` in production

##### Production mode
In production mode (default) are [http://localhost:8089/admin/info](http://localhost:8089/admin/info) and [http://localhost:8089/admin/health](http://localhost:8089/admin/health) accessible (through spring-boot-actuator) with basic auth `admin:admin`

## API Overview
Base path to the mashup API is [http://localhost:8080/cygni-music-api/](http://localhost:8080/cygni-music-api/)

[Swagger](https://swagger.io/) documentation of the API is available at [http://localhost:8080/cygni-music-api/swagger-ui.html](http://localhost:8080/cygni-music-api/swagger-ui.html)

<pre>
GET <b>/mashup/{mbid}</b>

Get mashup information by artist-id
Returns information about the artist
</pre>

<table>
	<tr><th colspan="3">Parameters</th></tr>
	<tr><th>Name</th><th>Type</th><th>Description</th></tr>
	<tr><td>mbid</td><td>String</td>
		<td>
			<a href="https://musicbrainz.org/doc/MusicBrainz_Identifier">MusicBranzID</a> of the artist, eg.
			<pre>d13f0f47-36f9-4661-87fe-2de56f45c649</pre>
		</td>
	</tr>
</table>
<table>
	<tr><th colspan="2">Responses</th></tr>
	<tr><th>Code</th><th>Description</th></tr>
	<tr>
		<td>200</td>
		<td>
			<pre>Successful</pre>
			<a href="#example-successful-response">Example response</a>
		</td>
	</tr>
	<tr>
		<td>400</td>
		<td>
			<pre>Invalid input</pre>
			The mbid is not valid, eg. not formatted correctly
		</td>
	</tr>
	<tr>
		<td>404</td>
		<td>
			<pre>Not found</pre>
			No artist with that mbid was found
		</td>
	</tr>
	<tr>
		<td>500</td>
		<td>
			<pre>Server error</pre>
			The server couldn't process the request
		</td>
	</tr>
	<tr>
		<td>503</td>
		<td>
			<pre>Subsystem error</pre>
			Underlying system couldn't process the request
		</td>
	</tr>
</table>

##### Example successful response
```javascript
{
  "mbid": "d13f0f47-36f9-4661-87fe-2de56f45c649",
  "description": "<p><b>Tegan and Sara</b> <span></span> are a Canadian indie pop band formed in 1998 in Calgary, Alberta, composed of identical twin sisters <b>Tegan Rain Quin</b> and <b>Sara Keirsten Quin</b> (born September 19, 1980). Both musicians are songwriters and multi-instrumentalists.</p>\n<p>The pair have released eight studio albums and numerous EPs. Their eighth album, <i>Love You to Death</i>, was released on June 3, 2016. The duo earned a Grammy nomination in 2013 for their DVD/Live Album \"Get Along\".</p>",
  "albums": [
    {
      "id": "64ebb8b3-6d69-3467-b458-27c3fd1df3b1",
      "title": "The Con",
      "imageUrl": "http://coverartarchive.org/release/7c5ef2ab-3f30-4fa0-8b70-61039f00a945/4809540564.jpg"
    },
    {
      "id": "71b7a4a4-f1a2-4fcc-a3a8-25829bf29680",
      "title": "Heartthrob",
      "imageUrl": "http://coverartarchive.org/release/9d46e2cc-1ae9-44aa-81bc-89e90eef410e/10240022979.jpg"
    },
    {
      "id": "b08e5e9a-b1dc-43d2-ba0a-0a0430939401",
      "title": "Sainthood",
      "imageUrl": "http://coverartarchive.org/release/5da5751a-4f95-3021-b5db-888b158a72a6/4809566954.jpg"
    },
  ]
}
```

## Description
This project is a programming exercise.

The API should mash information about an artist and present it for the user in one call.

* Description from [Wikipedia](https://www.wikipedia.org/)
* ImageUrl for albums

#### Background
The technical requirements for the exercise I summerized as:

* Use Java
* Build with Maven
* Run the application as an executable jar
* Production ready
* REST based API serving JSON

The solution of the exercise should also take in account

* Extendable (API and/or GUI)
* High load
* Quick response time (single user)

#### My solution
This was fun. Let's do it again!

###### Features (more or less implemented)

* Able to present the requested data
* Better response time (multithread for some http calls)
* Caching
* Exception handling
* Validation
* Unit tests
* Logging
* Production mode
* Some basic security
* Some configuration properties
* Documentation
	* README
	* Swagger
	* Some javadoc

###### Improvement needed

* Response time, high load
* Security
* Configurable properties

###### Features (not implemented)

* Order album (params)
* Extend with more subsystems
* Download the images (imageUrl)