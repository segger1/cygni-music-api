package se.johannalynn.exercise.cygnimusicapi.model;

import io.swagger.annotations.ApiModelProperty;

public class CygniMusicMashupAlbum {

    @ApiModelProperty(notes = "AlbumID", required = true)
    private String id;

    @ApiModelProperty(notes = "Title of the album")
    private String title;

    @ApiModelProperty(notes = "URL to fetch album image")
    private String imageUrl;

    public CygniMusicMashupAlbum(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
