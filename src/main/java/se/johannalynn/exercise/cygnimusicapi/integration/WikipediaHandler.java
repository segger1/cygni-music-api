package se.johannalynn.exercise.cygnimusicapi.integration;

import com.fasterxml.jackson.databind.JsonNode;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.ValueNotFoundException;

import java.util.Iterator;

public class WikipediaHandler extends IntegrationHandler<String> {

    public WikipediaHandler(String basePath) {
        super(basePath);
    }

    @Override
    public String getPath(String search) {
        return this.basePath + "?" + this.queryParams + search;
    }

    @Override
    public String handleResponse(JsonNode jsonNode) throws ValueNotFoundException {
        if(jsonNode != null && jsonNode.has("query")) {
            JsonNode query = jsonNode.get("query");
            if (jsonNode.get("query").has("pages")) {
                Iterator<JsonNode> itr = jsonNode.get("query").get("pages").elements();
                while (itr.hasNext()) {
                    JsonNode page = itr.next();
                    if(page.has("extract") && page.get("extract").isTextual()) {
                        return page.get("extract").asText();
                    }
                }
            }
        }
        throw new ValueNotFoundException("Wikipedia extract not found");
    }
}
