package se.johannalynn.exercise.cygnimusicapi.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CygniMusicApiValidation {

    private static final Pattern MBID_PATTERN = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");

    public static boolean validMbid(final String mbid) {
        if(mbid == null) return false;
        if(mbid.isEmpty()) return false;

        Matcher matcher = MBID_PATTERN.matcher(mbid);
        return matcher.matches();
    }

    public static boolean validURL(String path) {
        return path != null && !path.isEmpty() && (path.startsWith("http://") || path.startsWith("https://"));
    }
}
