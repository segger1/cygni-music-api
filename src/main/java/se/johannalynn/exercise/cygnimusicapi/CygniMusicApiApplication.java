package se.johannalynn.exercise.cygnimusicapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CygniMusicApiApplication {
    private static final Logger log = LoggerFactory.getLogger(CygniMusicApiApplication.class);

    public static void main(String[] args) {
        log.info("Startup application");
        SpringApplication.run(CygniMusicApiApplication.class, args);

    }

}
