package se.johannalynn.exercise.cygnimusicapi.web;

public class InvalidInputException extends RuntimeException {

    InvalidInputException(String message) {
        super(message);
    }
}
