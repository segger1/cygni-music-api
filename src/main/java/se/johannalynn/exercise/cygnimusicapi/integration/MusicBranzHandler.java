package se.johannalynn.exercise.cygnimusicapi.integration;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.ValueNotFoundException;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranz;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranzAlbum;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MusicBranzHandler extends IntegrationHandler<MusicBranz> {
    private static final Logger log = LoggerFactory.getLogger(MusicBranzHandler.class);

    public MusicBranzHandler(String basePath) {
        super(basePath);
    }

    @Override
    public String getPath(String search) {
        return this.basePath + search + "?" + this.queryParams;
    }

    @Override
    public MusicBranz handleResponse(JsonNode jsonNode) throws ValueNotFoundException {
        if(jsonNode == null || !jsonNode.has("id") || !jsonNode.get("id").isTextual()) {
            throw new ValueNotFoundException("MusicBranz id not found");
        }

        MusicBranz musicBranz = new MusicBranz(jsonNode.get("id").asText());

        if(jsonNode.has("relations") && jsonNode.get("relations").isArray()) {
            for (JsonNode rel : jsonNode.get("relations")) {
                if (rel.has("type") && rel.get("type").isTextual()
                        && rel.get("type").asText().equalsIgnoreCase("wikipedia")) {
                    if (rel.has("url") && rel.get("url").has("resource")
                            && rel.get("url").get("resource").isTextual()) {
                        String resource = rel.get("url").get("resource").asText();
                        int index = resource.lastIndexOf('/') + 1;
                        String titles = resource.substring(index);
                        musicBranz.setTitles(titles);
                    }
                }
            }
        }
        if(jsonNode.has("release-groups") && jsonNode.get("release-groups").isArray()) {
            Iterator<JsonNode> albumItr = jsonNode.get("release-groups").iterator();
            List<MusicBranzAlbum> albums = new ArrayList<>();
            while (albumItr.hasNext()) {
                JsonNode album = albumItr.next();
                String title = null;
                String id = null;
                if(album.has("title") && album.get("title").isTextual()) {
                    title = album.get("title").asText();
                }
                if(album.has("id") && album.get("id").isTextual()) {
                    id = album.get("id").asText();
                }
                albums.add(new MusicBranzAlbum(id, title));
            }
            musicBranz.setAlbums(albums);
        }
        return musicBranz;
    }
}
