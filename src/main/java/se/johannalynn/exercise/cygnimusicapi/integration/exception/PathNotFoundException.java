package se.johannalynn.exercise.cygnimusicapi.integration.exception;

public class PathNotFoundException extends NotFoundException {

    public PathNotFoundException(String message) {
        super(message);
    }
}
