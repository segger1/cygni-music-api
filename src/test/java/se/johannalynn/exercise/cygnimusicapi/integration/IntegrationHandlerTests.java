package se.johannalynn.exercise.cygnimusicapi.integration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.ValueNotFoundException;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranz;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranzAlbum;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class IntegrationHandlerTests {

    @Test
    public void parseCovertArt() throws IOException, ValueNotFoundException {
        final String coverartFile = "/coverart.json";

        InputStream is = this.getClass().getResourceAsStream(coverartFile);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(is);

        CoverArtHandler handler = new CoverArtHandler("");
        String imageUrl = handler.handleResponse(node);

        final String image = "http://coverartarchive.org/release/a146429a-cedc-3ab0-9e41-1aaf5f6cdc2d/3012495605.jpg";
        assertEquals(image, imageUrl);
    }

    @Test(expected = ValueNotFoundException.class)
    public void parseWithFailure() throws IOException, ValueNotFoundException {
        final String coverartFile = "/coverart-failure.json";

        InputStream is = this.getClass().getResourceAsStream(coverartFile);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(is);

        CoverArtHandler handler = new CoverArtHandler("");
        String imageUrl = handler.handleResponse(node);
    }

    @Test
    public void parseWikipedia() throws IOException, ValueNotFoundException {
        final String wikipediaFile = "/wikipedia.json";

        InputStream is = this.getClass().getResourceAsStream(wikipediaFile);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(is);

        WikipediaHandler handler = new WikipediaHandler("");
        String extract = handler.handleResponse(node);

        final String description = "<p><b>Nirvana</b> was an American Grunge and alternative rock band formed by singer and guitarist Kurt Cobain</p>";
        assertEquals(description, extract);
    }

    @Test
    public void parseMusicBranz() throws IOException, ValueNotFoundException {
        final String musicBranzFile = "/musicbranz.json";

        InputStream is = this.getClass().getResourceAsStream(musicBranzFile);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(is);

        MusicBranzHandler handler = new MusicBranzHandler("");
        MusicBranz musicBranz = handler.handleResponse(node);

        assertEquals("5b11f4ce-a62d-471e-81fc-a69a8278c7da", musicBranz.getId());
        assertEquals("Nirvana_(band)", musicBranz.getTitles());
        assertEquals(25, musicBranz.getAlbums().size());
        for(MusicBranzAlbum album : musicBranz.getAlbums()) {
            if(album.getTitle().equalsIgnoreCase("nevermind")) {
                assertEquals("1b022e01-4da6-387b-8658-8678046e4cef", album.getId());
                assertEquals("Nevermind", album.getTitle());
            }
        }
    }
}
