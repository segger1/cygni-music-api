package se.johannalynn.exercise.cygnimusicapi.service.exception;

public class ArtistNotFoundException extends RuntimeException {

    public ArtistNotFoundException(String message) {
        super(message);
    }
}
