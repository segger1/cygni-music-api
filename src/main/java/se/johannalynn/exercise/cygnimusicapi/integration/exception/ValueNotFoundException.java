package se.johannalynn.exercise.cygnimusicapi.integration.exception;

public class ValueNotFoundException extends NotFoundException {

    public ValueNotFoundException(String message) {
        super(message);
    }
}
