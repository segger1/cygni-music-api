package se.johannalynn.exercise.cygnimusicapi.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.SubsystemException;
import se.johannalynn.exercise.cygnimusicapi.service.exception.AlbumHandlingException;
import se.johannalynn.exercise.cygnimusicapi.service.exception.ArtistNotFoundException;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(InvalidInputException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    RestErrorInfo handleInvalidInputException(InvalidInputException e) {
        return response(e.getClass().getName(), e.getMessage());
    }

    @ExceptionHandler(ArtistNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    RestErrorInfo handleArtistNotFoundException(ArtistNotFoundException e){
        return response(e.getClass().getName(), e.getMessage());
    }

    @ExceptionHandler(SubsystemException.class)
    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    @ResponseBody
    RestErrorInfo handleSubsystemException(SubsystemException e) {
        return response(e.getClass().getName(), e.getMessage());
    }

    @ExceptionHandler(AlbumHandlingException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    RestErrorInfo handleAlbumHandlingException(AlbumHandlingException e) {
        return response(e.getClass().getName(), e.getMessage());
    }

    private RestErrorInfo response(String exception, String msg) {
        String exceptionName = exception.substring(exception.lastIndexOf('.') + 1);
        return new RestErrorInfo(exceptionName, msg);
    }
}
