package se.johannalynn.exercise.cygnimusicapi.integration;

import com.fasterxml.jackson.databind.JsonNode;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.ValueNotFoundException;

public class CoverArtHandler extends IntegrationHandler<String> {

    public CoverArtHandler(String basePath) {
        super(basePath);
    }

    @Override
    public String getPath(String search) {
        return this.basePath + search;
    }

    @Override
    public String handleResponse(JsonNode jsonNode) throws ValueNotFoundException {
        if(jsonNode != null && jsonNode.has("images") && jsonNode.get("images").isArray()) {
            for (JsonNode image : jsonNode.get("images")) {
                if (image.has("image") && image.get("image").isTextual()) {
                    return image.get("image").asText();
                }
            }
        }
        throw new ValueNotFoundException("Coverart imageUrl not found");
    }
}
