package se.johannalynn.exercise.cygnimusicapi.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import se.johannalynn.exercise.cygnimusicapi.integration.CoverArtHandler;
import se.johannalynn.exercise.cygnimusicapi.integration.MusicBranzHandler;
import se.johannalynn.exercise.cygnimusicapi.integration.SubsystemHttpClient;
import se.johannalynn.exercise.cygnimusicapi.integration.WikipediaHandler;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.NotFoundException;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashup;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashupAlbum;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranz;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranzAlbum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class MashupServiceTests {

    private static final String MBID = "d13f0f47-36f9-4661-87fe-2de56f45c649";

    @Autowired
    private MashupService unitUnderTest;

    @MockBean
    private SubsystemHttpClient httpClient;

    private void mockMusicBranz(String name, List<MusicBranzAlbum> albums) throws IOException, NotFoundException {
        MusicBranz musicBranz = new MusicBranz(MBID);
        musicBranz.setTitles(name);
        musicBranz.setAlbums(albums);
        when(httpClient.get(eq(MBID), any(MusicBranzHandler.class))).thenReturn(musicBranz);
    }

    private void mockWikipedia(String name, String description) throws IOException, NotFoundException {
        when(httpClient.get(eq(name), any(WikipediaHandler.class))).thenReturn(description);
    }

    private void mockCoverArt(String id, String image) throws IOException, NotFoundException {
        when(httpClient.get(eq(id), any(CoverArtHandler.class))).thenReturn(image);
    }

    @Test
    public void getMashupNoAlbum() throws IOException, NotFoundException {
        final String name = "ArtistA";
        final String description = "desc";
        mockMusicBranz(name, new ArrayList<>());
        mockWikipedia(name, description);

        CygniMusicMashup mashup = unitUnderTest.getMashup(MBID);

        assertEquals(MBID, mashup.getMbid());
        assertEquals(description, mashup.getDescription());
        assertEquals(0, mashup.getAlbums().size());
    }

    @Test
    public void handleSingleAlbums() throws IOException, NotFoundException {
        final String name = "ArtistA";
        final String description = "desc";

        List<MusicBranzAlbum> albums = new ArrayList<>();
        final String id1 = "001";
        final String title1 = "title1";
        MusicBranzAlbum album1 = new MusicBranzAlbum(id1, title1);
        albums.add(album1);

        final String image1 = "image1.jpg";

        mockMusicBranz(name, albums);
        mockWikipedia(name, description);
        mockCoverArt(id1, image1);

        CygniMusicMashup mashup = unitUnderTest.getMashup(MBID);
        List<CygniMusicMashupAlbum> mashupAlbums = mashup.getAlbums();

        assertEquals(1, mashupAlbums.size());
        assertEquals(image1, mashupAlbums.get(0).getImageUrl());
    }

    @Test
    public void handleAlbums() throws IOException, NotFoundException {
        final String name = "ArtistA";
        final String description = "desc";

        List<MusicBranzAlbum> albums = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            final String id = "id" + i;
            final String title = "title" + i;
            final String image = "image" + i + ".jpg";
            MusicBranzAlbum album = new MusicBranzAlbum(id, title);
            albums.add(album);
            mockCoverArt(id, image);
        }

        mockMusicBranz(name, albums);
        mockWikipedia(name, description);

        CygniMusicMashup mashup = unitUnderTest.getMashup(MBID);
        List<CygniMusicMashupAlbum> mashupAlbums = mashup.getAlbums();

        assertEquals(albums.size(), mashupAlbums.size());
        for(CygniMusicMashupAlbum album : mashupAlbums) {
            assertNotNull(album.getImageUrl());
        }
    }
}
