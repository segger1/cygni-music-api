package se.johannalynn.exercise.cygnimusicapi.service.exception;

public class AlbumHandlingException extends RuntimeException {

    public AlbumHandlingException(String message, InterruptedException throwable) {
        super(message, throwable);
    }
}
