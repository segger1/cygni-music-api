package se.johannalynn.exercise.cygnimusicapi.integration.exception;

import java.io.IOException;

public class SubsystemException extends RuntimeException {

    public SubsystemException(String message, IOException throwable) {
        super(message, throwable);
    }
}
