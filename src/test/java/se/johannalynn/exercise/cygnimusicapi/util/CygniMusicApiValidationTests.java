package se.johannalynn.exercise.cygnimusicapi.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class CygniMusicApiValidationTests {

    @Test
    public void validMbid() {
        assertTrue(CygniMusicApiValidation.validMbid("00000000-0000-0000-0000-000000000000"));
        assertTrue(CygniMusicApiValidation.validMbid("5b11f4ce-a62d-471e-81fc-a69a8278c7da"));
    }

    @Test
    public void invalidMbid() {
        assertFalse(CygniMusicApiValidation.validMbid(null));
        assertFalse(CygniMusicApiValidation.validMbid(""));
        assertFalse(CygniMusicApiValidation.validMbid("mbid"));
        assertFalse(CygniMusicApiValidation.validMbid("5b11f4ce-a62d-471e-81fc-a69a-8278-c7da"));
    }

    @Test
    public void validURL() {
        assertTrue(CygniMusicApiValidation.validURL("http://url"));
        assertTrue(CygniMusicApiValidation.validURL("https://url"));
    }

    @Test
    public void invalidURL() {
        assertFalse(CygniMusicApiValidation.validURL(null));
        assertFalse(CygniMusicApiValidation.validURL(""));
        assertFalse(CygniMusicApiValidation.validURL("blabla"));
        assertFalse(CygniMusicApiValidation.validURL("http"));
    }
}
