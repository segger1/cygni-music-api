package se.johannalynn.exercise.cygnimusicapi.model;

public class MusicBranzAlbum {

    private String id;
    private String title;

    public MusicBranzAlbum(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
