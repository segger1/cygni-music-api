package se.johannalynn.exercise.cygnimusicapi.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket mashupApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("se.johannalynn.exercise.cygnimusicapi.web"))
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Cygni Music API")
                .description("This project is a programming exercise.\n" +
                        "\n" +
                        "The API should mash information about an artist and present it for the user in one call.")
                .contact(new Contact("Johanna Lynn Lahti", "", "johanna.lynn.lahti@gmail.com"))
                .version("1.2")
                .build();
    }
}
