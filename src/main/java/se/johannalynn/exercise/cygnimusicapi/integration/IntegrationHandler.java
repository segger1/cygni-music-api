package se.johannalynn.exercise.cygnimusicapi.integration;

import com.fasterxml.jackson.databind.JsonNode;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.ValueNotFoundException;

public abstract class IntegrationHandler<T> {
    final String basePath;
    String queryParams = "";

    IntegrationHandler(final String basePath)  {
        this.basePath = basePath;
    }

    public void setQueryParams(String queryParams) {
        this.queryParams = queryParams;
    }

    /**
     * Get the full path for the integration
     *
     * @param search parameter for path
     * @return full path
     */
    public abstract String getPath(final String search);

    /**
     * Handle a json response and parse to the matching model
     *
     * @param jsonNode response as json tree node
     * @return response model of handler
     * @throws ValueNotFoundException when response model cannot be parsed from json
     */
    public abstract T handleResponse(JsonNode jsonNode) throws ValueNotFoundException;
}
