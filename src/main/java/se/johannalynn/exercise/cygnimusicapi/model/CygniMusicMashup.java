package se.johannalynn.exercise.cygnimusicapi.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class CygniMusicMashup {

    @ApiModelProperty(notes = "MusicBranzID of the artist", required = true)
    private String mbid;

    @ApiModelProperty(notes = "Artist description from Wikipedia")
    private String description;

    @ApiModelProperty(notes = "Albums by the artist")
    private List<CygniMusicMashupAlbum> albums;

    public String getMbid() {
        return mbid;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CygniMusicMashupAlbum> getAlbums() {
        return albums;
    }

    public void setAlbums(List<CygniMusicMashupAlbum> albums) {
        this.albums = albums;
    }
}
