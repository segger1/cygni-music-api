package se.johannalynn.exercise.cygnimusicapi.configuration;


import org.apache.http.HttpHost;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class IntegrationConfiguration {
    private static final Logger log = LoggerFactory.getLogger(IntegrationConfiguration.class);

    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager(
            @Value("${http.cm.max-connection}") int maxConnection,
            @Value("${http.cm.route-max-connection}") int routeMaxConnection) {
        log.debug("PoolingHttpClientConnectionManager [ max-connection=" + maxConnection + ", route-max-connection=" + routeMaxConnection + " ]");
        PoolingHttpClientConnectionManager connectionManager
                = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(maxConnection);
        connectionManager.setDefaultMaxPerRoute(routeMaxConnection);

        HttpHost localhost = new HttpHost("localhost", 8080);
        connectionManager.setMaxPerRoute(new HttpRoute(localhost), routeMaxConnection);
        return connectionManager;
    }

    @Bean
    public CloseableHttpClient closeableHttpClient(
            PoolingHttpClientConnectionManager connectionManager,
            @Value("${http.client.user-agent}") String userAgent) {
        log.debug("ClosableHttpClient [ user-agent=" + userAgent + " ]");
        return CachingHttpClients.custom()
                .setCacheConfig(CacheConfig.DEFAULT)
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(
                        RequestConfig.custom().
                                setCookieSpec(CookieSpecs.STANDARD).build())
                .setUserAgent(userAgent)
                .build();
    }

    @Bean
    public ExecutorService executorService(@Value("${executor.poolsize}") int numberOfThreads) {
        log.debug("ExecutorService [ poolsize=" + numberOfThreads + " ]");
        return Executors.newFixedThreadPool(numberOfThreads);
    }
}
