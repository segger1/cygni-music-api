package se.johannalynn.exercise.cygnimusicapi.integration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.NotFoundException;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.PathNotFoundException;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.ResponseNotFoundException;
import se.johannalynn.exercise.cygnimusicapi.util.CygniMusicApiValidation;

import javax.annotation.PreDestroy;
import java.io.IOException;

@Service
public class SubsystemHttpClient {
    private static Logger log = LoggerFactory.getLogger(SubsystemHttpClient.class);

    @Autowired
    private CloseableHttpClient client;

    @Autowired
    private ObjectMapper mapper;

    @PreDestroy
    public void cleanUp() {
        try {
            client.close();
        } catch (IOException e) {
            log.error("Couldn't close httpclient", e.getMessage());
        }
    }

    private HttpGet getHttpGetRequest(final String path) {
        HttpGet httpGet = new HttpGet(path);
        httpGet.setHeader("Accept","application/json");
        return httpGet;
    }

    /**
     * Execute a GET request
     * Validate path input and http response
     *
     * @param search parameter for request
     * @param handler of request and response
     * @param <T> response model type of the handler
     * @return response model of the handler
     * @throws IOException when subsystem couldn't handle the request
     * @throws NotFoundException when request or response is not well handled
     */
    public <T> T get(final String search , IntegrationHandler<T> handler) throws IOException, NotFoundException {
        log.debug("Got search parameter [ %s ]", search);
        if(search == null || search.isEmpty()) throw new PathNotFoundException("No search parameter");

        final String path = handler.getPath(search);
        if(!CygniMusicApiValidation.validURL(path)) throw new PathNotFoundException("Non valid path [ " + path + " ]");

        log.debug("Got request path [ %s ]", path);
        HttpGet httpGet = getHttpGetRequest(path);

        long start = System.currentTimeMillis();
        long stop = System.currentTimeMillis();
        log.debug("Got response in " + (stop-start) + " ms");

        try (CloseableHttpResponse response = client.execute(httpGet)) {
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() >= 300) {
                throw new ResponseNotFoundException("Erroneous status [ statusCode=" + status.getStatusCode() + ", reason=" + status.getReasonPhrase() + " ]");
            }

            HttpEntity entity = response.getEntity();
            if (entity == null) {
                throw new ResponseNotFoundException("No body in response");
            }

            JsonNode node = mapper.readTree(entity.getContent());
            return handler.handleResponse(node);
        } finally {
            httpGet.releaseConnection();
        }
    }
}
