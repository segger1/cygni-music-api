package se.johannalynn.exercise.cygnimusicapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.johannalynn.exercise.cygnimusicapi.service.MashupService;
import se.johannalynn.exercise.cygnimusicapi.web.CygniMusicApiController;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CygniMusicApiApplicationTests {

    @Autowired
    private CygniMusicApiController controller;

    @Autowired
    private MashupService service;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
        assertThat(service).isNotNull();
    }
}
