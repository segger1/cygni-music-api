package se.johannalynn.exercise.cygnimusicapi.integration.exception;

public class ResponseNotFoundException extends NotFoundException {

    public ResponseNotFoundException(String message) {
        super(message);
    }
}
