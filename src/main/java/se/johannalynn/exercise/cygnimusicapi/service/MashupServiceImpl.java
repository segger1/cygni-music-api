package se.johannalynn.exercise.cygnimusicapi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import se.johannalynn.exercise.cygnimusicapi.integration.CoverArtHandler;
import se.johannalynn.exercise.cygnimusicapi.integration.MusicBranzHandler;
import se.johannalynn.exercise.cygnimusicapi.integration.SubsystemHttpClient;
import se.johannalynn.exercise.cygnimusicapi.integration.WikipediaHandler;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.NotFoundException;
import se.johannalynn.exercise.cygnimusicapi.integration.exception.SubsystemException;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashup;
import se.johannalynn.exercise.cygnimusicapi.model.CygniMusicMashupAlbum;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranz;
import se.johannalynn.exercise.cygnimusicapi.model.MusicBranzAlbum;
import se.johannalynn.exercise.cygnimusicapi.service.exception.AlbumHandlingException;
import se.johannalynn.exercise.cygnimusicapi.service.exception.ArtistNotFoundException;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Service
public class MashupServiceImpl implements MashupService {
    private static final Logger log = LoggerFactory.getLogger(MashupServiceImpl.class);

    private final MusicBranzHandler musicBranzHandler;
    private final WikipediaHandler wikipediaHandler;
    private final CoverArtHandler coverArtHandler;

    @Autowired
    private ExecutorService threadPoolService;

    @Autowired
    private SubsystemHttpClient client;

    public MashupServiceImpl(
            @Value("${integration.base-path.music-branz}") final String musicBranzBasePath,
            @Value("${integration.query-params.music-branz}") final String musicBranzQueryParams,
            @Value("${integration.base-path.wikipedia}") final String wikipediaBasePath,
            @Value("${integration.query-params.wikipedia}") final String wikipediaQueryParams,
            @Value("${integration.base-path.coverart}") final String covertBasePath) {

        //TODO: autowire handlers with paths?
        musicBranzHandler = new MusicBranzHandler(musicBranzBasePath);
        musicBranzHandler.setQueryParams(musicBranzQueryParams);
        wikipediaHandler = new WikipediaHandler(wikipediaBasePath);
        wikipediaHandler.setQueryParams(wikipediaQueryParams);
        coverArtHandler = new CoverArtHandler(covertBasePath);
    }

    @PreDestroy
    public void cleanUp() {
        threadPoolService.shutdown();
        try {
            threadPoolService.awaitTermination(100, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error("Couldn't terminate threadpoolservice", e.getMessage());
        }
    }

    private MusicBranz getMusicBranz(final String mbid) throws ArtistNotFoundException, SubsystemException {
        try {
            return client.get(mbid, musicBranzHandler);
        } catch (NotFoundException e) {
            throw new ArtistNotFoundException("Artist [" + mbid + "] not found");
        } catch (IOException e) {
            throw new SubsystemException("MusicBranz couldn't serve request", e);
        }
    }

    private String getDescription(String resource) throws SubsystemException {
        try {
            return client.get(resource, wikipediaHandler);
        } catch (NotFoundException e) {
            log.error("Wikipedia couldn't find description", e.getMessage());
            return null;
        } catch (IOException e) {
            throw new SubsystemException("Wikipedia couldn't serve request", e);
        }
    }

    @Cacheable("mashup")
    public CygniMusicMashup getMashup(final String mbid) throws SubsystemException, AlbumHandlingException, ArtistNotFoundException {
        long start = System.currentTimeMillis();
        CygniMusicMashup mashup = new CygniMusicMashup();
        try {
            MusicBranz musicBranz = getMusicBranz(mbid);
            mashup.setMbid(mbid);

            String resource = musicBranz.getTitles();
            String description = getDescription(resource);
            mashup.setDescription(description);

            List<CygniMusicMashupAlbum> albums = handleAlbums(musicBranz.getAlbums());
            mashup.setAlbums(albums);
        } catch (InterruptedException e) {
            throw new AlbumHandlingException("Thread interrupted during imageUrl search", e);
        }
        log.info("Got mashup in " + (System.currentTimeMillis() - start) + " ms");
        return mashup;
    }

    /**
     * Ask for album information as a separate task to optimize the retrieval
     * @param albums from subsystem
     * @return mashup albums
     * @throws InterruptedException when thread handling goes wrong
     */
    private List<CygniMusicMashupAlbum> handleAlbums(List<MusicBranzAlbum> albums) throws InterruptedException {
        if(albums.isEmpty()) {
            return new ArrayList<>();
        }

        List<CygniMusicMashupAlbum> albumList = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(albums.size());
        for (MusicBranzAlbum mbAlbum : albums) {
            String id = mbAlbum.getId();
            CygniMusicMashupAlbum album = new CygniMusicMashupAlbum(id, mbAlbum.getTitle());
            populateImageUrl(latch, album, id);
            albumList.add(album);
        }
        latch.await();

        return albumList;
    }

    /**
     * Retrieve the imageUrl in a separate thread. Update the consumer when the thread is finished.
     * @param latch for counting threads
     * @param album for adding image url
     * @param imageId to find image url for
     */
    private void populateImageUrl(CountDownLatch latch, CygniMusicMashupAlbum album, final String imageId) {
        Consumer<String> callback = imageUrl -> {
            album.setImageUrl(imageUrl);
            latch.countDown();
        };

        threadPoolService.execute(() -> {
            try {
                String imageUrl = client.get(imageId, coverArtHandler);
                callback.accept(imageUrl);
            } catch (NotFoundException e) {
                log.warn("Couldn't find imageUrl", e.getMessage());
                callback.accept(null);
            } catch (IOException e) {
                log.error("Couldn't retrieve imageUrl [%s]", imageId);
            }
        });
    }
}
