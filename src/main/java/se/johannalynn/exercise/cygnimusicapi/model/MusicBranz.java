package se.johannalynn.exercise.cygnimusicapi.model;

import java.util.List;

public class MusicBranz {

    private String id;
    private String titles;
    private List<MusicBranzAlbum> albums;

    public MusicBranz(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public List<MusicBranzAlbum> getAlbums() {
        return albums;
    }

    public void setAlbums(List<MusicBranzAlbum> albums) {
        this.albums = albums;
    }
}
